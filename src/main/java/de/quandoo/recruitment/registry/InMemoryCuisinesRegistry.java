package de.quandoo.recruitment.registry;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	/*
	 Using Set as Data Structure to hold Customers, 
	 Pros: Takes cares of Duplicates, Efficient "Contains" check 
	 Cons: Resizing of the set if there are a lot of add operations
	 */
	private Map<String, Set<Customer> > customerRegistry = new HashMap<>();

	private static final String ITALIAN_CUISINE = "italian";
	private static final String FRENCH_CUISINE = "french";
	private static final String GERMAN_CUISINE = "german";

	//Add a new Cuisine here 
	private Set<String> availableCuisines = new HashSet<>( Arrays.asList(ITALIAN_CUISINE,FRENCH_CUISINE, GERMAN_CUISINE ) );

	private String cuisineName;

	@Override
	public void register(final Customer userId, final Cuisine cuisine) {

		cuisineName = cuisine.getName().toLowerCase().intern();

		if( availableCuisines.contains( cuisineName ) ) 
		{
			Set<Customer> value = new HashSet<>();
			value.add(userId);
			customerRegistry.merge( cuisineName, value, ( oldValue, newValue ) -> { oldValue.addAll(newValue); return oldValue; } );
		}
		else 
		{
			System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
		}
	}

	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		
		List<Customer> cuisineCustomers = new LinkedList<>();

		if( cuisine == null || cuisine.getName() == null )
			return cuisineCustomers;

		cuisineName = cuisine.getName().toLowerCase();
		cuisineCustomers.addAll( customerRegistry.get( cuisineName ) );
		
		return cuisineCustomers;
	}

	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		List<Cuisine> customerCuisines = new LinkedList<>();
		
		//Search Customer in all Cuisine Lists, and Prepare a cusines list accordingly
		customerRegistry.entrySet().stream().filter( p -> p.getValue().contains( customer ) ).map( m -> new Cuisine( m.getKey() ) ).forEach( x -> customerCuisines.add( x ) );
		
		return customerCuisines;
	}

	//Assuming this is to return top 'n' cuisines
	@Override
	public List<Cuisine> topCuisines(final int n) {

		Map<Integer,String> topCuisines = new TreeMap<>(Collections.reverseOrder());

		customerRegistry.forEach( (k,v) -> topCuisines.put( v.size(), k ) );

		return topCuisines.entrySet().stream().limit( n ).map( e -> new Cuisine( e.getValue() ) ).collect( Collectors.toList() ); 
	}
}
