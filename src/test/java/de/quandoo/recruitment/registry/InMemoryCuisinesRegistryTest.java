package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.List;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();
    private InMemoryCuisinesRegistry topCuisinesRegistry;

    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
    }

    @Test
    public void shouldWork2() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork3() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test(expected = RuntimeException.class)
    @Ignore
    public void thisDoesntWorkYet() {
        cuisinesRegistry.topCuisines(1);
    }

    @Test
    public void IfCuisinesMatchForCustomer_ThenPass()
    {
    	cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
    	cuisinesRegistry.register(new Customer("5"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));

        List<Customer> customers = cuisinesRegistry.cuisineCustomers( new Cuisine("french") );
        assertThat( customers, hasSize(3) );
        assertThat( customers, containsInAnyOrder(new Customer("1"), new Customer("2"), new Customer("5") ) );
    }
    
    @Test
    public void ifCustomerInMultipleCusines_thenPass() 
    {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("italian"));

        List<Cuisine> customerCuisines = cuisinesRegistry.customerCuisines(new Customer("1"));
        assertThat( customerCuisines, hasItems( new Cuisine("italian"), new Cuisine("french") ) );
    }
    
    //Tests for Top Functionality
    
    @Before
    public void prepareRegistry() 
    {
    	topCuisinesRegistry = new InMemoryCuisinesRegistry();
    	topCuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
    	topCuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
    	topCuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
    	topCuisinesRegistry.register(new Customer("4"), new Cuisine("italian"));
    	topCuisinesRegistry.register(new Customer("5"), new Cuisine("french"));
    	topCuisinesRegistry.register(new Customer("6"), new Cuisine("french"));
    	topCuisinesRegistry.register(new Customer("7"), new Cuisine("italian"));
    	topCuisinesRegistry.register(new Customer("8"), new Cuisine("italian"));
    }
    
    @Test
    public void ifTopOrderIsCorrect_thenPass() 
    {
        int top = 3;
    	List<Cuisine> topCuisines = topCuisinesRegistry.topCuisines(top);
        
        assertThat( topCuisines, hasSize(top) );
        assertThat( topCuisines,contains(new Cuisine("italian"),new Cuisine("french"),new Cuisine("german")) );

    }
    
    @Test
    public void ifItalianIsTopCuisine_thenPass() 
    {
        int top = 1;
    	List<Cuisine> topCuisines = topCuisinesRegistry.topCuisines(top);
        
        assertThat( topCuisines, hasSize(top) );
        assertThat( topCuisines, hasItem(new Cuisine("italian")) );

    }
    
    @Test
    public void ifItalianIsTopAndFrenchIsSecondCuisine_thenPass() 
    {
    	int top = 2;
        List<Cuisine> topCuisines = topCuisinesRegistry.topCuisines(top);
        
        assertThat( topCuisines, hasSize(top) );
        assertThat( topCuisines, contains( new Cuisine("italian"), new Cuisine("french") ) );

    }
    
}